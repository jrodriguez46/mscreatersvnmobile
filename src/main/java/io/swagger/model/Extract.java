package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * the extract task resource for resource pool management
 */
@ApiModel(description = "the extract task resource for resource pool management")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Extract   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("resourcePool")
  private ResourcePoolRef resourcePool = null;

  public Extract id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the extract task for resource pool Management
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the extract task for resource pool Management")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Extract resourcePool(ResourcePoolRef resourcePool) {
    this.resourcePool = resourcePool;
    return this;
  }

  /**
   * Get resourcePool
   * @return resourcePool
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ResourcePoolRef getResourcePool() {
    return resourcePool;
  }

  public void setResourcePool(ResourcePoolRef resourcePool) {
    this.resourcePool = resourcePool;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Extract extract = (Extract) o;
    return Objects.equals(this.id, extract.id) &&
        Objects.equals(this.resourcePool, extract.resourcePool);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, resourcePool);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Extract {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    resourcePool: ").append(toIndentedString(resourcePool)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

