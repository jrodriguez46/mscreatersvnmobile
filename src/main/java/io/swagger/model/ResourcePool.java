package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * manages resource capacity with the resource state
 */
@ApiModel(description = "manages resource capacity with the resource state")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourcePool   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("capacity")
  @Valid
  private List<Capacity> capacity = null;

  @JsonProperty("relatedParty")
  private RelatedPartyRef relatedParty = null;

  @JsonProperty("productOfferingRef")
  private ProductOfferingRef productOfferingRef = null;

  @JsonProperty("resourceCollection")
  @Valid
  private List<ResourceCollectionRef> resourceCollection = null;

  public ResourcePool id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the Resource Pool.
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the Resource Pool.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ResourcePool name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the resource pool
   * @return name
  **/
  @ApiModelProperty(value = "Name of the resource pool")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ResourcePool description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A string. free-text description of the Resource Pool.
   * @return description
  **/
  @ApiModelProperty(value = "A string. free-text description of the Resource Pool.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ResourcePool type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type of the resource pool
   * @return type
  **/
  @ApiModelProperty(value = "Type of the resource pool")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ResourcePool capacity(List<Capacity> capacity) {
    this.capacity = capacity;
    return this;
  }

  public ResourcePool addCapacityItem(Capacity capacityItem) {
    if (this.capacity == null) {
      this.capacity = new ArrayList<Capacity>();
    }
    this.capacity.add(capacityItem);
    return this;
  }

  /**
   * Get capacity
   * @return capacity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Capacity> getCapacity() {
    return capacity;
  }

  public void setCapacity(List<Capacity> capacity) {
    this.capacity = capacity;
  }

  public ResourcePool relatedParty(RelatedPartyRef relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public RelatedPartyRef getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(RelatedPartyRef relatedParty) {
    this.relatedParty = relatedParty;
  }

  public ResourcePool productOfferingRef(ProductOfferingRef productOfferingRef) {
    this.productOfferingRef = productOfferingRef;
    return this;
  }

  /**
   * Get productOfferingRef
   * @return productOfferingRef
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ProductOfferingRef getProductOfferingRef() {
    return productOfferingRef;
  }

  public void setProductOfferingRef(ProductOfferingRef productOfferingRef) {
    this.productOfferingRef = productOfferingRef;
  }

  public ResourcePool resourceCollection(List<ResourceCollectionRef> resourceCollection) {
    this.resourceCollection = resourceCollection;
    return this;
  }

  public ResourcePool addResourceCollectionItem(ResourceCollectionRef resourceCollectionItem) {
    if (this.resourceCollection == null) {
      this.resourceCollection = new ArrayList<ResourceCollectionRef>();
    }
    this.resourceCollection.add(resourceCollectionItem);
    return this;
  }

  /**
   * Get resourceCollection
   * @return resourceCollection
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceCollectionRef> getResourceCollection() {
    return resourceCollection;
  }

  public void setResourceCollection(List<ResourceCollectionRef> resourceCollection) {
    this.resourceCollection = resourceCollection;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourcePool resourcePool = (ResourcePool) o;
    return Objects.equals(this.id, resourcePool.id) &&
        Objects.equals(this.name, resourcePool.name) &&
        Objects.equals(this.description, resourcePool.description) &&
        Objects.equals(this.type, resourcePool.type) &&
        Objects.equals(this.capacity, resourcePool.capacity) &&
        Objects.equals(this.relatedParty, resourcePool.relatedParty) &&
        Objects.equals(this.productOfferingRef, resourcePool.productOfferingRef) &&
        Objects.equals(this.resourceCollection, resourcePool.resourceCollection);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, type, capacity, relatedParty, productOfferingRef, resourceCollection);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourcePool {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    capacity: ").append(toIndentedString(capacity)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    productOfferingRef: ").append(toIndentedString(productOfferingRef)).append("\n");
    sb.append("    resourceCollection: ").append(toIndentedString(resourceCollection)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

