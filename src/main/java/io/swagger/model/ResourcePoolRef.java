package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * manages resource capacity with the resource state
 */
@ApiModel(description = "manages resource capacity with the resource state")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourcePoolRef   {
  @JsonProperty("href")
  private String href = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("relatedParty")
  private RelatedPartyRef relatedParty = null;

  @JsonProperty("resourceCollection")
  @Valid
  private List<ResourceCollectionRef> resourceCollection = null;

  @JsonProperty("productOfferingRef")
  private ProductOfferingRef productOfferingRef = null;

  public ResourcePoolRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * A string. Hyperlink to access the Resource Pool.
   * @return href
  **/
  @ApiModelProperty(value = "A string. Hyperlink to access the Resource Pool.")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public ResourcePoolRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the Resource Pool.
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the Resource Pool.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ResourcePoolRef relatedParty(RelatedPartyRef relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public RelatedPartyRef getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(RelatedPartyRef relatedParty) {
    this.relatedParty = relatedParty;
  }

  public ResourcePoolRef resourceCollection(List<ResourceCollectionRef> resourceCollection) {
    this.resourceCollection = resourceCollection;
    return this;
  }

  public ResourcePoolRef addResourceCollectionItem(ResourceCollectionRef resourceCollectionItem) {
    if (this.resourceCollection == null) {
      this.resourceCollection = new ArrayList<ResourceCollectionRef>();
    }
    this.resourceCollection.add(resourceCollectionItem);
    return this;
  }

  /**
   * Get resourceCollection
   * @return resourceCollection
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceCollectionRef> getResourceCollection() {
    return resourceCollection;
  }

  public void setResourceCollection(List<ResourceCollectionRef> resourceCollection) {
    this.resourceCollection = resourceCollection;
  }

  public ResourcePoolRef productOfferingRef(ProductOfferingRef productOfferingRef) {
    this.productOfferingRef = productOfferingRef;
    return this;
  }

  /**
   * Get productOfferingRef
   * @return productOfferingRef
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ProductOfferingRef getProductOfferingRef() {
    return productOfferingRef;
  }

  public void setProductOfferingRef(ProductOfferingRef productOfferingRef) {
    this.productOfferingRef = productOfferingRef;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourcePoolRef resourcePoolRef = (ResourcePoolRef) o;
    return Objects.equals(this.href, resourcePoolRef.href) &&
        Objects.equals(this.id, resourcePoolRef.id) &&
        Objects.equals(this.relatedParty, resourcePoolRef.relatedParty) &&
        Objects.equals(this.resourceCollection, resourcePoolRef.resourceCollection) &&
        Objects.equals(this.productOfferingRef, resourcePoolRef.productOfferingRef);
  }

  @Override
  public int hashCode() {
    return Objects.hash(href, id, relatedParty, resourceCollection, productOfferingRef);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourcePoolRef {\n");
    
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    resourceCollection: ").append(toIndentedString(resourceCollection)).append("\n");
    sb.append("    productOfferingRef: ").append(toIndentedString(productOfferingRef)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

