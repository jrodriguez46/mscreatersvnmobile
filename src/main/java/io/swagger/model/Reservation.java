package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * reservation api resource
 */
@ApiModel(description = "reservation api resource")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reservation   {
  @JsonProperty("@baseType")
  private String baseType = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("@type")
  private String type = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("reservationState")
  private String reservationState = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyRef> relatedParty = null;

  @JsonProperty("reservationItem")
  @Valid
  private List<ReservationItem> reservationItem = null;

  @JsonProperty("validFor")
  private TimePeriod validFor = null;

  public Reservation baseType(String baseType) {
    this.baseType = baseType;
    return this;
  }

  /**
   * Get baseType
   * @return baseType
  **/
  @ApiModelProperty(value = "")


  public String getBaseType() {
    return baseType;
  }

  public void setBaseType(String baseType) {
    this.baseType = baseType;
  }

  public Reservation schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A string. Generic attribute containing the link to the schema that defines the structure of the class type of the current object.
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A string. Generic attribute containing the link to the schema that defines the structure of the class type of the current object.")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public Reservation type(String type) {
    this.type = type;
    return this;
  }

  /**
   * A string. Indicates the (class) type of reservation. Ex. serviceItemReservation, resourceItemReservation
   * @return type
  **/
  @ApiModelProperty(value = "A string. Indicates the (class) type of reservation. Ex. serviceItemReservation, resourceItemReservation")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Reservation description(String description) {
    this.description = description;
    return this;
  }

  /**
   * A string. free-text description of the reservation.
   * @return description
  **/
  @ApiModelProperty(value = "A string. free-text description of the reservation.")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Reservation href(String href) {
    this.href = href;
    return this;
  }

  /**
   * A string. Hyperlink to access the reservation.
   * @return href
  **/
  @ApiModelProperty(value = "A string. Hyperlink to access the reservation.")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Reservation id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the Reservation.
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the Reservation.")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Reservation reservationState(String reservationState) {
    this.reservationState = reservationState;
    return this;
  }

  /**
   * A string. The life cycle state of the reservation.
   * @return reservationState
  **/
  @ApiModelProperty(value = "A string. The life cycle state of the reservation.")


  public String getReservationState() {
    return reservationState;
  }

  public void setReservationState(String reservationState) {
    this.reservationState = reservationState;
  }

  public Reservation relatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public Reservation addRelatedPartyItem(RelatedPartyRef relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyRef>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<RelatedPartyRef> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public Reservation reservationItem(List<ReservationItem> reservationItem) {
    this.reservationItem = reservationItem;
    return this;
  }

  public Reservation addReservationItemItem(ReservationItem reservationItemItem) {
    if (this.reservationItem == null) {
      this.reservationItem = new ArrayList<ReservationItem>();
    }
    this.reservationItem.add(reservationItemItem);
    return this;
  }

  /**
   * Get reservationItem
   * @return reservationItem
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ReservationItem> getReservationItem() {
    return reservationItem;
  }

  public void setReservationItem(List<ReservationItem> reservationItem) {
    this.reservationItem = reservationItem;
  }

  public Reservation validFor(TimePeriod validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * The period for which the catalog is valid
   * @return validFor
  **/
  @ApiModelProperty(value = "The period for which the catalog is valid")

  @Valid

  public TimePeriod getValidFor() {
    return validFor;
  }

  public void setValidFor(TimePeriod validFor) {
    this.validFor = validFor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reservation reservation = (Reservation) o;
    return Objects.equals(this.baseType, reservation.baseType) &&
        Objects.equals(this.schemaLocation, reservation.schemaLocation) &&
        Objects.equals(this.type, reservation.type) &&
        Objects.equals(this.description, reservation.description) &&
        Objects.equals(this.href, reservation.href) &&
        Objects.equals(this.id, reservation.id) &&
        Objects.equals(this.reservationState, reservation.reservationState) &&
        Objects.equals(this.relatedParty, reservation.relatedParty) &&
        Objects.equals(this.reservationItem, reservation.reservationItem) &&
        Objects.equals(this.validFor, reservation.validFor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(baseType, schemaLocation, type, description, href, id, reservationState, relatedParty, reservationItem, validFor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Reservation {\n");
    
    sb.append("    baseType: ").append(toIndentedString(baseType)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    reservationState: ").append(toIndentedString(reservationState)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    reservationItem: ").append(toIndentedString(reservationItem)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

