package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * Specific ability of an entity measured in quantity and units of quantity over an extended period.
 */
@ApiModel(description = "Specific ability of an entity measured in quantity and units of quantity over an extended period.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Capacity   {
  @JsonProperty("capacityAmount")
  private Quantity capacityAmount = null;

  @JsonProperty("appliedCapacitylist")
  @Valid
  private List<AppliedCapacityAmount> appliedCapacitylist = null;

  @JsonProperty("applicableTimePeriod")
  private ApplicableTimePeriod applicableTimePeriod = null;

  public Capacity capacityAmount(Quantity capacityAmount) {
    this.capacityAmount = capacityAmount;
    return this;
  }

  /**
   * Get capacityAmount
   * @return capacityAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Quantity getCapacityAmount() {
    return capacityAmount;
  }

  public void setCapacityAmount(Quantity capacityAmount) {
    this.capacityAmount = capacityAmount;
  }

  public Capacity appliedCapacitylist(List<AppliedCapacityAmount> appliedCapacitylist) {
    this.appliedCapacitylist = appliedCapacitylist;
    return this;
  }

  public Capacity addAppliedCapacitylistItem(AppliedCapacityAmount appliedCapacitylistItem) {
    if (this.appliedCapacitylist == null) {
      this.appliedCapacitylist = new ArrayList<AppliedCapacityAmount>();
    }
    this.appliedCapacitylist.add(appliedCapacitylistItem);
    return this;
  }

  /**
   * Get appliedCapacitylist
   * @return appliedCapacitylist
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<AppliedCapacityAmount> getAppliedCapacitylist() {
    return appliedCapacitylist;
  }

  public void setAppliedCapacitylist(List<AppliedCapacityAmount> appliedCapacitylist) {
    this.appliedCapacitylist = appliedCapacitylist;
  }

  public Capacity applicableTimePeriod(ApplicableTimePeriod applicableTimePeriod) {
    this.applicableTimePeriod = applicableTimePeriod;
    return this;
  }

  /**
   * Get applicableTimePeriod
   * @return applicableTimePeriod
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ApplicableTimePeriod getApplicableTimePeriod() {
    return applicableTimePeriod;
  }

  public void setApplicableTimePeriod(ApplicableTimePeriod applicableTimePeriod) {
    this.applicableTimePeriod = applicableTimePeriod;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Capacity capacity = (Capacity) o;
    return Objects.equals(this.capacityAmount, capacity.capacityAmount) &&
        Objects.equals(this.appliedCapacitylist, capacity.appliedCapacitylist) &&
        Objects.equals(this.applicableTimePeriod, capacity.applicableTimePeriod);
  }

  @Override
  public int hashCode() {
    return Objects.hash(capacityAmount, appliedCapacitylist, applicableTimePeriod);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Capacity {\n");
    
    sb.append("    capacityAmount: ").append(toIndentedString(capacityAmount)).append("\n");
    sb.append("    appliedCapacitylist: ").append(toIndentedString(appliedCapacitylist)).append("\n");
    sb.append("    applicableTimePeriod: ").append(toIndentedString(applicableTimePeriod)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

