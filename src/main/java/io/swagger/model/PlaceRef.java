package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

/**
 * Used to define a place useful for the resource Used to indicate reserved resources
 */
@ApiModel(description = "Used to define a place useful for the resource Used to indicate reserved resources")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceRef   {
  @JsonProperty("@referredType")
  private String referredType = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("role")
  private String role = null;

  public PlaceRef referredType(String referredType) {
    this.referredType = referredType;
    return this;
  }

  /**
   * A string. Indicates the type of the referred object. This attribute is to be used when the object is representing a reference to an existing object instead of the of the object itself.
   * @return referredType
  **/
  @ApiModelProperty(value = "A string. Indicates the type of the referred object. This attribute is to be used when the object is representing a reference to an existing object instead of the of the object itself.")


  public String getReferredType() {
    return referredType;
  }

  public void setReferredType(String referredType) {
    this.referredType = referredType;
  }

  public PlaceRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * A string. Hyperlink to access the Place.
   * @return href
  **/
  @ApiModelProperty(value = "A string. Hyperlink to access the Place.")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public PlaceRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the Place
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the Place")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PlaceRef name(String name) {
    this.name = name;
    return this;
  }

  /**
   * A string. Name of the Place.
   * @return name
  **/
  @ApiModelProperty(value = "A string. Name of the Place.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PlaceRef role(String role) {
    this.role = role;
    return this;
  }

  /**
   * Get role
   * @return role
  **/
  @ApiModelProperty(value = "")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PlaceRef placeRef = (PlaceRef) o;
    return Objects.equals(this.referredType, placeRef.referredType) &&
        Objects.equals(this.href, placeRef.href) &&
        Objects.equals(this.id, placeRef.id) &&
        Objects.equals(this.name, placeRef.name) &&
        Objects.equals(this.role, placeRef.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(referredType, href, id, name, role);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PlaceRef {\n");
    
    sb.append("    referredType: ").append(toIndentedString(referredType)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

