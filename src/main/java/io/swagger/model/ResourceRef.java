package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * ResourceRef
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceRef   {
  @JsonProperty("description")
  private String description = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("characteristic")
  @Valid
  private List<ResourceCharacteristic> characteristic = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyRef> relatedParty = null;

  @JsonProperty("category")
  @Valid
  private List<CategoryRef> category = null;

  public ResourceRef description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the resource
   * @return description
  **/
  @ApiModelProperty(value = "Description of the resource")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ResourceRef href(String href) {
    this.href = href;
    return this;
  }

  /**
   * Get href
   * @return href
  **/
  @ApiModelProperty(value = "")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public ResourceRef id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ResourceRef name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the Resource
   * @return name
  **/
  @ApiModelProperty(value = "Name of the Resource")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ResourceRef state(String state) {
    this.state = state;
    return this;
  }

  /**
   * lifecycle of the resource
   * @return state
  **/
  @ApiModelProperty(value = "lifecycle of the resource")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public ResourceRef type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type of the resource
   * @return type
  **/
  @ApiModelProperty(value = "Type of the resource")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ResourceRef characteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public ResourceRef addCharacteristicItem(ResourceCharacteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<ResourceCharacteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceCharacteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
  }

  public ResourceRef relatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public ResourceRef addRelatedPartyItem(RelatedPartyRef relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyRef>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<RelatedPartyRef> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public ResourceRef category(List<CategoryRef> category) {
    this.category = category;
    return this;
  }

  public ResourceRef addCategoryItem(CategoryRef categoryItem) {
    if (this.category == null) {
      this.category = new ArrayList<CategoryRef>();
    }
    this.category.add(categoryItem);
    return this;
  }

  /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<CategoryRef> getCategory() {
    return category;
  }

  public void setCategory(List<CategoryRef> category) {
    this.category = category;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourceRef resourceRef = (ResourceRef) o;
    return Objects.equals(this.description, resourceRef.description) &&
        Objects.equals(this.href, resourceRef.href) &&
        Objects.equals(this.id, resourceRef.id) &&
        Objects.equals(this.name, resourceRef.name) &&
        Objects.equals(this.state, resourceRef.state) &&
        Objects.equals(this.type, resourceRef.type) &&
        Objects.equals(this.characteristic, resourceRef.characteristic) &&
        Objects.equals(this.relatedParty, resourceRef.relatedParty) &&
        Objects.equals(this.category, resourceRef.category);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, href, id, name, state, type, characteristic, relatedParty, category);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourceRef {\n");
    
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

