package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * an aggregate entity consisting of ResourceElement
 */
@ApiModel(description = "an aggregate entity consisting of ResourceElement")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceCollectionRef   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("objectId")
  private String objectId = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("characteristic")
  @Valid
  private List<ResourceCharacteristic> characteristic = null;

  public ResourceCollectionRef name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Represents a user-friendly identifier of an object.
   * @return name
  **/
  @ApiModelProperty(value = "Represents a user-friendly identifier of an object.")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ResourceCollectionRef objectId(String objectId) {
    this.objectId = objectId;
    return this;
  }

  /**
   * A list of consisted Resources.
   * @return objectId
  **/
  @ApiModelProperty(value = "A list of consisted Resources.")


  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

  public ResourceCollectionRef type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Name of the Resource type
   * @return type
  **/
  @ApiModelProperty(value = "Name of the Resource type")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ResourceCollectionRef state(String state) {
    this.state = state;
    return this;
  }

  /**
   * lifecycle of the resource
   * @return state
  **/
  @ApiModelProperty(value = "lifecycle of the resource")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public ResourceCollectionRef characteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public ResourceCollectionRef addCharacteristicItem(ResourceCharacteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<ResourceCharacteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceCharacteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourceCollectionRef resourceCollectionRef = (ResourceCollectionRef) o;
    return Objects.equals(this.name, resourceCollectionRef.name) &&
        Objects.equals(this.objectId, resourceCollectionRef.objectId) &&
        Objects.equals(this.type, resourceCollectionRef.type) &&
        Objects.equals(this.state, resourceCollectionRef.state) &&
        Objects.equals(this.characteristic, resourceCollectionRef.characteristic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, objectId, type, state, characteristic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourceCollectionRef {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    objectId: ").append(toIndentedString(objectId)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

