package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * the amount of capacity that is planned to be consumed or has been consumed.
 */
@ApiModel(description = "the amount of capacity that is planned to be consumed or has been consumed.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceCapacityDemand   {
  @JsonProperty("capacityDemandAmount")
  private Quantity capacityDemandAmount = null;

  @JsonProperty("resourcePool")
  private ResourcePoolRef resourcePool = null;

  @JsonProperty("place")
  @Valid
  private List<PlaceRef> place = null;

  public ResourceCapacityDemand capacityDemandAmount(Quantity capacityDemandAmount) {
    this.capacityDemandAmount = capacityDemandAmount;
    return this;
  }

  /**
   * Get capacityDemandAmount
   * @return capacityDemandAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Quantity getCapacityDemandAmount() {
    return capacityDemandAmount;
  }

  public void setCapacityDemandAmount(Quantity capacityDemandAmount) {
    this.capacityDemandAmount = capacityDemandAmount;
  }

  public ResourceCapacityDemand resourcePool(ResourcePoolRef resourcePool) {
    this.resourcePool = resourcePool;
    return this;
  }

  /**
   * Get resourcePool
   * @return resourcePool
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ResourcePoolRef getResourcePool() {
    return resourcePool;
  }

  public void setResourcePool(ResourcePoolRef resourcePool) {
    this.resourcePool = resourcePool;
  }

  public ResourceCapacityDemand place(List<PlaceRef> place) {
    this.place = place;
    return this;
  }

  public ResourceCapacityDemand addPlaceItem(PlaceRef placeItem) {
    if (this.place == null) {
      this.place = new ArrayList<PlaceRef>();
    }
    this.place.add(placeItem);
    return this;
  }

  /**
   * Get place
   * @return place
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<PlaceRef> getPlace() {
    return place;
  }

  public void setPlace(List<PlaceRef> place) {
    this.place = place;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResourceCapacityDemand resourceCapacityDemand = (ResourceCapacityDemand) o;
    return Objects.equals(this.capacityDemandAmount, resourceCapacityDemand.capacityDemandAmount) &&
        Objects.equals(this.resourcePool, resourceCapacityDemand.resourcePool) &&
        Objects.equals(this.place, resourceCapacityDemand.place);
  }

  @Override
  public int hashCode() {
    return Objects.hash(capacityDemandAmount, resourcePool, place);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResourceCapacityDemand {\n");
    
    sb.append("    capacityDemandAmount: ").append(toIndentedString(capacityDemandAmount)).append("\n");
    sb.append("    resourcePool: ").append(toIndentedString(resourcePool)).append("\n");
    sb.append("    place: ").append(toIndentedString(place)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

