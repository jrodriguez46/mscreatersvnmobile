package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * The amount of CapcityDemand applied to a CapacityAmount.
 */
@ApiModel(description = "The amount of CapcityDemand applied to a CapacityAmount.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppliedCapacityAmount   {
  @JsonProperty("appliedDemandAmount")
  private Quantity appliedDemandAmount = null;

  @JsonProperty("resourceCapacityDemand")
  private ResourceCapacityDemand resourceCapacityDemand = null;

  @JsonProperty("resource")
  @Valid
  private List<ResourceRef> resource = null;

  public AppliedCapacityAmount appliedDemandAmount(Quantity appliedDemandAmount) {
    this.appliedDemandAmount = appliedDemandAmount;
    return this;
  }

  /**
   * Get appliedDemandAmount
   * @return appliedDemandAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public Quantity getAppliedDemandAmount() {
    return appliedDemandAmount;
  }

  public void setAppliedDemandAmount(Quantity appliedDemandAmount) {
    this.appliedDemandAmount = appliedDemandAmount;
  }

  public AppliedCapacityAmount resourceCapacityDemand(ResourceCapacityDemand resourceCapacityDemand) {
    this.resourceCapacityDemand = resourceCapacityDemand;
    return this;
  }

  /**
   * Get resourceCapacityDemand
   * @return resourceCapacityDemand
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ResourceCapacityDemand getResourceCapacityDemand() {
    return resourceCapacityDemand;
  }

  public void setResourceCapacityDemand(ResourceCapacityDemand resourceCapacityDemand) {
    this.resourceCapacityDemand = resourceCapacityDemand;
  }

  public AppliedCapacityAmount resource(List<ResourceRef> resource) {
    this.resource = resource;
    return this;
  }

  public AppliedCapacityAmount addResourceItem(ResourceRef resourceItem) {
    if (this.resource == null) {
      this.resource = new ArrayList<ResourceRef>();
    }
    this.resource.add(resourceItem);
    return this;
  }

  /**
   * Get resource
   * @return resource
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceRef> getResource() {
    return resource;
  }

  public void setResource(List<ResourceRef> resource) {
    this.resource = resource;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AppliedCapacityAmount appliedCapacityAmount = (AppliedCapacityAmount) o;
    return Objects.equals(this.appliedDemandAmount, appliedCapacityAmount.appliedDemandAmount) &&
        Objects.equals(this.resourceCapacityDemand, appliedCapacityAmount.resourceCapacityDemand) &&
        Objects.equals(this.resource, appliedCapacityAmount.resource);
  }

  @Override
  public int hashCode() {
    return Objects.hash(appliedDemandAmount, resourceCapacityDemand, resource);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AppliedCapacityAmount {\n");
    
    sb.append("    appliedDemandAmount: ").append(toIndentedString(appliedDemandAmount)).append("\n");
    sb.append("    resourceCapacityDemand: ").append(toIndentedString(resourceCapacityDemand)).append("\n");
    sb.append("    resource: ").append(toIndentedString(resource)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

