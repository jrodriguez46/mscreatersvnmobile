package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * An identified part of the reservation. A reservation is decomposed into one or more reservation items.
 */
@ApiModel(description = "An identified part of the reservation. A reservation is decomposed into one or more reservation items.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReservationItem   {
  @JsonProperty("@baseType")
  private String baseType = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("@type")
  private String type = null;

  @JsonProperty("quantity")
  private Integer quantity = null;

  @JsonProperty("subReservationState")
  private String subReservationState = null;

  @JsonProperty("action")
  private String action = null;

  @JsonProperty("resourceCapacity")
  private ResourceCapacityDemand resourceCapacity = null;

  @JsonProperty("appliedCapacityAmount")
  private AppliedCapacityAmount appliedCapacityAmount = null;

  public ReservationItem baseType(String baseType) {
    this.baseType = baseType;
    return this;
  }

  /**
   * Get baseType
   * @return baseType
  **/
  @ApiModelProperty(value = "")


  public String getBaseType() {
    return baseType;
  }

  public void setBaseType(String baseType) {
    this.baseType = baseType;
  }

  public ReservationItem schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * Get schemaLocation
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public ReservationItem type(String type) {
    this.type = type;
    return this;
  }

  /**
   * A string. Generic attribute containing the name of the resource class type.
   * @return type
  **/
  @ApiModelProperty(value = "A string. Generic attribute containing the name of the resource class type.")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public ReservationItem quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Represents the number of reservationItems that make up the reservation.
   * @return quantity
  **/
  @ApiModelProperty(value = "Represents the number of reservationItems that make up the reservation.")


  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public ReservationItem subReservationState(String subReservationState) {
    this.subReservationState = subReservationState;
    return this;
  }

  /**
   * A string. The life cycle state of the each reservation item.
   * @return subReservationState
  **/
  @ApiModelProperty(value = "A string. The life cycle state of the each reservation item.")


  public String getSubReservationState() {
    return subReservationState;
  }

  public void setSubReservationState(String subReservationState) {
    this.subReservationState = subReservationState;
  }

  public ReservationItem action(String action) {
    this.action = action;
    return this;
  }

  /**
   * Get action
   * @return action
  **/
  @ApiModelProperty(value = "")


  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public ReservationItem resourceCapacity(ResourceCapacityDemand resourceCapacity) {
    this.resourceCapacity = resourceCapacity;
    return this;
  }

  /**
   * Get resourceCapacity
   * @return resourceCapacity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ResourceCapacityDemand getResourceCapacity() {
    return resourceCapacity;
  }

  public void setResourceCapacity(ResourceCapacityDemand resourceCapacity) {
    this.resourceCapacity = resourceCapacity;
  }

  public ReservationItem appliedCapacityAmount(AppliedCapacityAmount appliedCapacityAmount) {
    this.appliedCapacityAmount = appliedCapacityAmount;
    return this;
  }

  /**
   * Get appliedCapacityAmount
   * @return appliedCapacityAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AppliedCapacityAmount getAppliedCapacityAmount() {
    return appliedCapacityAmount;
  }

  public void setAppliedCapacityAmount(AppliedCapacityAmount appliedCapacityAmount) {
    this.appliedCapacityAmount = appliedCapacityAmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReservationItem reservationItem = (ReservationItem) o;
    return Objects.equals(this.baseType, reservationItem.baseType) &&
        Objects.equals(this.schemaLocation, reservationItem.schemaLocation) &&
        Objects.equals(this.type, reservationItem.type) &&
        Objects.equals(this.quantity, reservationItem.quantity) &&
        Objects.equals(this.subReservationState, reservationItem.subReservationState) &&
        Objects.equals(this.action, reservationItem.action) &&
        Objects.equals(this.resourceCapacity, reservationItem.resourceCapacity) &&
        Objects.equals(this.appliedCapacityAmount, reservationItem.appliedCapacityAmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(baseType, schemaLocation, type, quantity, subReservationState, action, resourceCapacity, appliedCapacityAmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReservationItem {\n");
    
    sb.append("    baseType: ").append(toIndentedString(baseType)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    subReservationState: ").append(toIndentedString(subReservationState)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    resourceCapacity: ").append(toIndentedString(resourceCapacity)).append("\n");
    sb.append("    appliedCapacityAmount: ").append(toIndentedString(appliedCapacityAmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

