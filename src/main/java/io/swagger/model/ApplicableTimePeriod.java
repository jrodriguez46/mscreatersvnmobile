package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ec.com.claro.mscreatersvnmobile.util.CustomerDeserializer;
import ec.com.claro.mscreatersvnmobile.util.CustomerSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * The period of time for which Capacity or CapacityDemand applies.
 */
@ApiModel(description = "The period of time for which Capacity or CapacityDemand applies.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicableTimePeriod   {
  @JsonProperty("dayOfWeek")
  private String dayOfWeek = null;

  @JsonProperty("fromToDateTime")
  @JsonSerialize(using = CustomerSerializer.class)
  @JsonDeserialize(using = CustomerDeserializer.class)
  private OffsetDateTime fromToDateTime = null;

  @JsonProperty("rangeInterval")
  private String rangeInterval = null;

  @JsonProperty("validFor")
  private String validFor = null;

  public ApplicableTimePeriod dayOfWeek(String dayOfWeek) {
    this.dayOfWeek = dayOfWeek;
    return this;
  }

  /**
   * A day or days representing when the schedule is applicable. For example 2, 3 represent Monday and Tuesday.
   * @return dayOfWeek
  **/
  @ApiModelProperty(value = "A day or days representing when the schedule is applicable. For example 2, 3 represent Monday and Tuesday.")


  public String getDayOfWeek() {
    return dayOfWeek;
  }

  public void setDayOfWeek(String dayOfWeek) {
    this.dayOfWeek = dayOfWeek;
  }

  public ApplicableTimePeriod fromToDateTime(OffsetDateTime fromToDateTime) {
    this.fromToDateTime = fromToDateTime;
    return this;
  }

  /**
   * The period of time for which the schedule is applicable.  Instance values are mutually exclusive with daysOfWeek values.
   * @return fromToDateTime
  **/
  @ApiModelProperty(value = "The period of time for which the schedule is applicable.  Instance values are mutually exclusive with daysOfWeek values.")

  @Valid

  public OffsetDateTime getFromToDateTime() {
    return fromToDateTime;
  }

  public void setFromToDateTime(OffsetDateTime fromToDateTime) {
    this.fromToDateTime = fromToDateTime;
  }

  public ApplicableTimePeriod rangeInterval(String rangeInterval) {
    this.rangeInterval = rangeInterval;
    return this;
  }

  /**
   * An indicator that specifies the inclusion or exclusion of the from and to DateTime attributes.  Possible values are \"open\", \"closed\", \"closedBottom\" and \"closedTop\".
   * @return rangeInterval
  **/
  @ApiModelProperty(value = "An indicator that specifies the inclusion or exclusion of the from and to DateTime attributes.  Possible values are \"open\", \"closed\", \"closedBottom\" and \"closedTop\".")


  public String getRangeInterval() {
    return rangeInterval;
  }

  public void setRangeInterval(String rangeInterval) {
    this.rangeInterval = rangeInterval;
  }

  public ApplicableTimePeriod validFor(String validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * The period of time during which the schedule is considered of interest to the business.
   * @return validFor
  **/
  @ApiModelProperty(value = "The period of time during which the schedule is considered of interest to the business.")


  public String getValidFor() {
    return validFor;
  }

  public void setValidFor(String validFor) {
    this.validFor = validFor;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApplicableTimePeriod applicableTimePeriod = (ApplicableTimePeriod) o;
    return Objects.equals(this.dayOfWeek, applicableTimePeriod.dayOfWeek) &&
        Objects.equals(this.fromToDateTime, applicableTimePeriod.fromToDateTime) &&
        Objects.equals(this.rangeInterval, applicableTimePeriod.rangeInterval) &&
        Objects.equals(this.validFor, applicableTimePeriod.validFor);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dayOfWeek, fromToDateTime, rangeInterval, validFor);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApplicableTimePeriod {\n");
    
    sb.append("    dayOfWeek: ").append(toIndentedString(dayOfWeek)).append("\n");
    sb.append("    fromToDateTime: ").append(toIndentedString(fromToDateTime)).append("\n");
    sb.append("    rangeInterval: ").append(toIndentedString(rangeInterval)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

