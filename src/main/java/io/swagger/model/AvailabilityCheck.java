package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;

/**
 * the availabilityCheck task resource for resource pool management
 */
@ApiModel(description = "the availabilityCheck task resource for resource pool management")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-09-17T15:23:55.174Z")

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AvailabilityCheck   {
  @JsonProperty("href")
  private String href = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("@type")
  private String type = null;

  @JsonProperty("@schemaLocation")
  private String schemaLocation = null;

  @JsonProperty("@baseType")
  private String baseType = null;

  @JsonProperty("resourceCapacity")
  private ResourceCapacityDemand resourceCapacity = null;

  @JsonProperty("appliedCapacityAmount")
  private AppliedCapacityAmount appliedCapacityAmount = null;

  @JsonProperty("characteristic")
  @Valid
  private List<ResourceCharacteristic> characteristic = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyRef> relatedParty = null;

  public AvailabilityCheck href(String href) {
    this.href = href;
    return this;
  }

  /**
   * A string. Hyperlink to access the availabilityCheck task for resource pool Management
   * @return href
  **/
  @ApiModelProperty(value = "A string. Hyperlink to access the availabilityCheck task for resource pool Management")


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public AvailabilityCheck id(String id) {
    this.id = id;
    return this;
  }

  /**
   * A string. Identifier of an instance of the availabilityCheck task for resource pool Management
   * @return id
  **/
  @ApiModelProperty(value = "A string. Identifier of an instance of the availabilityCheck task for resource pool Management")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public AvailabilityCheck type(String type) {
    this.type = type;
    return this;
  }

  /**
   * The class type of the actual resource (for type extension).
   * @return type
  **/
  @ApiModelProperty(value = "The class type of the actual resource (for type extension).")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public AvailabilityCheck schemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
    return this;
  }

  /**
   * A link to the schema describing a resource (for type extension).
   * @return schemaLocation
  **/
  @ApiModelProperty(value = "A link to the schema describing a resource (for type extension).")


  public String getSchemaLocation() {
    return schemaLocation;
  }

  public void setSchemaLocation(String schemaLocation) {
    this.schemaLocation = schemaLocation;
  }

  public AvailabilityCheck baseType(String baseType) {
    this.baseType = baseType;
    return this;
  }

  /**
   * The base type for use in polymorphic collections
   * @return baseType
  **/
  @ApiModelProperty(value = "The base type for use in polymorphic collections")


  public String getBaseType() {
    return baseType;
  }

  public void setBaseType(String baseType) {
    this.baseType = baseType;
  }

  public AvailabilityCheck resourceCapacity(ResourceCapacityDemand resourceCapacity) {
    this.resourceCapacity = resourceCapacity;
    return this;
  }

  /**
   * Get resourceCapacity
   * @return resourceCapacity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ResourceCapacityDemand getResourceCapacity() {
    return resourceCapacity;
  }

  public void setResourceCapacity(ResourceCapacityDemand resourceCapacity) {
    this.resourceCapacity = resourceCapacity;
  }

  public AvailabilityCheck appliedCapacityAmount(AppliedCapacityAmount appliedCapacityAmount) {
    this.appliedCapacityAmount = appliedCapacityAmount;
    return this;
  }

  /**
   * Get appliedCapacityAmount
   * @return appliedCapacityAmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AppliedCapacityAmount getAppliedCapacityAmount() {
    return appliedCapacityAmount;
  }

  public void setAppliedCapacityAmount(AppliedCapacityAmount appliedCapacityAmount) {
    this.appliedCapacityAmount = appliedCapacityAmount;
  }

  public AvailabilityCheck characteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
    return this;
  }

  public AvailabilityCheck addCharacteristicItem(ResourceCharacteristic characteristicItem) {
    if (this.characteristic == null) {
      this.characteristic = new ArrayList<ResourceCharacteristic>();
    }
    this.characteristic.add(characteristicItem);
    return this;
  }

  /**
   * Get characteristic
   * @return characteristic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ResourceCharacteristic> getCharacteristic() {
    return characteristic;
  }

  public void setCharacteristic(List<ResourceCharacteristic> characteristic) {
    this.characteristic = characteristic;
  }

  public AvailabilityCheck relatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public AvailabilityCheck addRelatedPartyItem(RelatedPartyRef relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyRef>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * Get relatedParty
   * @return relatedParty
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<RelatedPartyRef> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyRef> relatedParty) {
    this.relatedParty = relatedParty;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvailabilityCheck availabilityCheck = (AvailabilityCheck) o;
    return Objects.equals(this.href, availabilityCheck.href) &&
        Objects.equals(this.id, availabilityCheck.id) &&
        Objects.equals(this.type, availabilityCheck.type) &&
        Objects.equals(this.schemaLocation, availabilityCheck.schemaLocation) &&
        Objects.equals(this.baseType, availabilityCheck.baseType) &&
        Objects.equals(this.resourceCapacity, availabilityCheck.resourceCapacity) &&
        Objects.equals(this.appliedCapacityAmount, availabilityCheck.appliedCapacityAmount) &&
        Objects.equals(this.characteristic, availabilityCheck.characteristic) &&
        Objects.equals(this.relatedParty, availabilityCheck.relatedParty);
  }

  @Override
  public int hashCode() {
    return Objects.hash(href, id, type, schemaLocation, baseType, resourceCapacity, appliedCapacityAmount, characteristic, relatedParty);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AvailabilityCheck {\n");
    
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    schemaLocation: ").append(toIndentedString(schemaLocation)).append("\n");
    sb.append("    baseType: ").append(toIndentedString(baseType)).append("\n");
    sb.append("    resourceCapacity: ").append(toIndentedString(resourceCapacity)).append("\n");
    sb.append("    appliedCapacityAmount: ").append(toIndentedString(appliedCapacityAmount)).append("\n");
    sb.append("    characteristic: ").append(toIndentedString(characteristic)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

