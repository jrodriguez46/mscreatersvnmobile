package ec.com.claro.mscreatersvnmobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import co.elastic.apm.attach.ElasticApmAttacher;

@SpringBootApplication(scanBasePackages = "ec.com.claro.mscreatersvnmobile")
public class Application{

	public static void main(String[] args) {
		ElasticApmAttacher.attach();
		SpringApplication.run(Application.class, args);
	}

}
