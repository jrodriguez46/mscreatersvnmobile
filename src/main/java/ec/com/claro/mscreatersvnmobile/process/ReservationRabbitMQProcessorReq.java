package ec.com.claro.mscreatersvnmobile.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscreatersvnmobile.util.CommonMappings;
import io.swagger.model.Reservation;

public class ReservationRabbitMQProcessorReq implements Processor {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("inicia");
		//Save Headers CommonMappings
		CommonMappings commonMappings = new CommonMappings();
		commonMappings.setHeaderRequest(exchange);
		
		
		String reservationRes = "";
		reservationRes = exchange.getIn().getBody(String.class);
		
		String httpCodeDat = "";
		httpCodeDat = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE) +"";
		exchange.setProperty("RespondeDat", reservationRes);
		exchange.setProperty("httpCodeDat", httpCodeDat);
		
		Reservation reservation = new Reservation();
		reservation = exchange.getProperty("request",(Reservation.class));
		
		String jsonRetrieveOrganizationReq = new ObjectMapper().writeValueAsString(reservation);
	
		String logBodyReq = "Body request legacy >>>>> " + jsonRetrieveOrganizationReq;
		log.info(logBodyReq);
		
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
		exchange.setProperty("BodySRE", jsonRetrieveOrganizationReq);
		exchange.getOut().setBody(jsonRetrieveOrganizationReq);
	}

}
