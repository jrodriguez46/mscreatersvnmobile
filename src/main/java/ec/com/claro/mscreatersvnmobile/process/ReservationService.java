package ec.com.claro.mscreatersvnmobile.process;


import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import ec.com.claro.mscreatersvnmobile.exception.PayloadException;
import ec.com.claro.mscreatersvnmobile.util.CommonMappings;
import io.swagger.model.GenericFault;
import io.swagger.model.Reservation;

public class ReservationService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private static final String MESSAGE_FAULT = "messageFault";
	

	public Reservation mappingRes(Exchange exchange)
			throws ParserConfigurationException, SAXException, IOException {

		log.info("mappingRes  mappingRes Star  >>>>> ");
		// Objetos
		Reservation reservation = new Reservation();
		
		CommonMappings commonMappings = new CommonMappings();
		String resLegacyLog = "Headers >>>>> " + exchange.getIn().getHeaders().toString();
		
		String bodyResponseLegacyRabbit = exchange.getIn().getBody(String.class);
		String bodyResponseLegacy = "";
		String httpCode = "";
		
		if(exchange.getProperty("RespondeDat", String.class) != null) {
			bodyResponseLegacy = exchange.getProperty("RespondeDat", String.class);
		}else {
			bodyResponseLegacy = bodyResponseLegacyRabbit;
		}
		
		
		if(exchange.getProperty("httpCodeDat", String.class) != null) {
			httpCode = exchange.getProperty("httpCodeDat", String.class);
		}else {
			httpCode = exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE)+"";
		}
				exchange.getProperty("RespondeDat", String.class);
				
		log.info("Body Rabbit" + bodyResponseLegacyRabbit +"Body Dat " +bodyResponseLegacy);

		if (httpCode.toString().equalsIgnoreCase("200")) {

			
		} else {
			
			String pvMsjError = "";
			String codeError="";
			String actor="";
			
			log.info("exchange "+ httpCode );
			
			GenericFault generic =commonMappings.parseResFault(bodyResponseLegacy);
			if (generic.getErrorList() != null) {
					pvMsjError = generic.getErrorList().getError().get(0).getBusinessMeaning();
					codeError=generic.getErrorList().getError().get(0).getCode();
					actor=generic.getErrorList().getError().get(0).getActor();

			}
			commonMappings.saveProperty(exchange, pvMsjError, codeError,httpCode,actor);
			exchange.setProperty(MESSAGE_FAULT,pvMsjError );
			throw new PayloadException(exchange.getProperty(MESSAGE_FAULT)+ "");
		}

		return reservation;
	}
}
