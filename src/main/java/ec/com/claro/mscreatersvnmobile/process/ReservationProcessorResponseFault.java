package ec.com.claro.mscreatersvnmobile.process;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.http.common.HttpMessage;
import org.threeten.bp.OffsetDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.model.DetailResType;
import io.swagger.model.ErrorListType;
import io.swagger.model.GenericFault;

public class ReservationProcessorResponseFault implements Processor {
	
	private static final String REQUEST_DATE = "requestDate";
	private static final String MESSAGE_ID = "messageId";
	private static final String CODE_FAULT = "codeFault";
	private static final String SEVERITY_LVL_1 = "1";
	private static final String MESSAGE_FAULT = "messageFault";
	private static final String ACTOR = "actor";
	
	@Override
	public void process(Exchange exchange) throws Exception {
		GenericFault genericFault = new GenericFault();
		ErrorListType errorListType = new ErrorListType();
		List<DetailResType> listDetailResType = new ArrayList<>();
		DetailResType detailResType = new DetailResType();
		
		OffsetDateTime requestDate = (OffsetDateTime) exchange.getProperty(REQUEST_DATE);

		genericFault.setMessageUUID(exchange.getProperty(MESSAGE_ID).toString());
		OffsetDateTime dateTime = OffsetDateTime.now();
		genericFault.setResponseDate(dateTime);
		genericFault.setLatency(dateTime.getSecond() - requestDate.getSecond());
		detailResType.setCode(exchange.getProperty(CODE_FAULT).toString());
		detailResType.setSeverityLevel(SEVERITY_LVL_1);
		
		
		
		detailResType.setDescription( exchange.getIn().getHeaders() + " body"+ exchange.getIn().getBody());
		detailResType.setActor(exchange.getProperty(ACTOR).toString());
		detailResType.setBusinessMeaning(exchange.getProperty(MESSAGE_FAULT)+"");
		listDetailResType.add(detailResType);
		errorListType.setError(listDetailResType);
		genericFault.setErrorList(errorListType);
		
		// Respuesta fault
		OffsetDateTime responseDate = OffsetDateTime.now();
		
		exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, exchange.getProperty("codeHttp").toString());
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		exchange.getOut().setHeader("correlatorId", exchange.getProperty("correlatorId"));
		exchange.getOut().setHeader("country", exchange.getProperty("country"));
		exchange.getOut().setHeader("originSystem", exchange.getProperty("originSystem"));
		exchange.getOut().setHeader(MESSAGE_ID, exchange.getProperty(MESSAGE_ID));
		exchange.getOut().setHeader("responseDate", responseDate);
		exchange.getOut().setHeader("targetSystem", exchange.getProperty("targetSystem"));
		
		String json = new ObjectMapper().writeValueAsString(genericFault);
		exchange.getOut(HttpMessage.class).setBody(json);


	}

}
