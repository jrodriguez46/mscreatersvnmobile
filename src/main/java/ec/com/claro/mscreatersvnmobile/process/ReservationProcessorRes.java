package ec.com.claro.mscreatersvnmobile.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.threeten.bp.OffsetDateTime;

import io.swagger.model.Reservation;


public class ReservationProcessorRes implements Processor {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		log.info("DocumentProcessorRes Star >>>>> ");
		
		Reservation reservation;
		log.info("mappingRes Star >>>>> ");
		ReservationService reservationService = new ReservationService();
		reservation = reservationService.mappingRes(exchange);
		
		exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, exchange.getIn().getHeader(Exchange.HTTP_RESPONSE_CODE));
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		
		exchange.getOut().setBody(reservation);
		
		//Header response
		OffsetDateTime responseDate = OffsetDateTime.now();
		exchange.getOut().setHeader("correlatorId", exchange.getProperty("correlatorId"));
		exchange.getOut().setHeader("country", exchange.getProperty("country"));
		exchange.getOut().setHeader("messageId", exchange.getProperty("messageId"));
		exchange.getOut().setHeader("responseDate", responseDate);
		exchange.getOut().setHeader("targetSystem", exchange.getProperty("targetSystem"));

	}

}
