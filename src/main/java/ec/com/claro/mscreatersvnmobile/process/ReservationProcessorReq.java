package ec.com.claro.mscreatersvnmobile.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import ec.com.claro.mscreatersvnmobile.util.CommonMappings;
import io.swagger.model.Reservation;

public class ReservationProcessorReq implements Processor {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("inicia");
		//Save Headers CommonMappings
		CommonMappings commonMappings = new CommonMappings();
		commonMappings.saveHeaders(exchange);
		commonMappings.saveProperty(exchange);
		commonMappings.setHeaderRequest(exchange);
		
		log.info("mappinf");
		
		Reservation reservation = new Reservation();
		reservation = exchange.getIn().getBody(Reservation.class);
		
		exchange.setProperty("request", reservation);	
		//maping
		
		String jsonRetrieveOrganizationReq = new ObjectMapper().writeValueAsString(reservation);
	
		String logBodyReq = "Body request legacy >>>>> " + jsonRetrieveOrganizationReq;
		log.info(logBodyReq);
		
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
		exchange.getOut().setBody(jsonRetrieveOrganizationReq);
	}

}
