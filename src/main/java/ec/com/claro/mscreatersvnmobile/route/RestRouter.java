/*CR 33, 25.11.21, AF*/
package ec.com.claro.mscreatersvnmobile.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import ec.com.claro.mscreatersvnmobile.process.ReservationProcessorReq;
import ec.com.claro.mscreatersvnmobile.process.ReservationProcessorRes;
import ec.com.claro.mscreatersvnmobile.process.ReservationProcessorResponseFault;
import io.swagger.model.Reservation;


@Component
public class RestRouter extends RouteBuilder {
	
	//String MSReservationMovilAddEndpoint = System.getenv().get("ENDPOINT_REST_RESERVATION_MOVIL") + "?throwExceptionOnFailure=false";

	String MSReservationMovilAddEndpoint = "http://localhost:8093/MS/ENT/ResourcePoolManagement/RSCreateRsvnMobileDB/v1/reservation"+"?throwExceptionOnFailure=false";
		
	@Override
    public void configure() throws Exception {
    	restConfiguration()
        	.contextPath("/reservation").apiContextPath("/reservation/api-camel")
        	.apiProperty("api.title", "Camel REST API Resource Pool Management")
        	.apiProperty("api.version", "1.0")
        	.apiProperty("cors", "true")
        	.apiContextRouteId("document")
        	.bindingMode(RestBindingMode.json)
        	.dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES");

            rest("/reservation")
        	.produces("application/json")
        	.consumes("application/json")
        	.post("/").type(Reservation.class).outType(Reservation.class).route().routeId("reservation")
        	.doTry()
	        	.log("inica el mapeo del SRE")
	        	.process(new ReservationProcessorReq())
		    	.log("Inicia el To al SRE")
		    	.to(MSReservationMovilAddEndpoint)
		    	.log("Sale del llamado al SRE")
		    
		    	.process(new ReservationProcessorRes())
		    .endDoTry()	
        	.doCatch(Exception.class)
        	
        	.process(new ReservationProcessorResponseFault())
        	.endRest();
    }
}

