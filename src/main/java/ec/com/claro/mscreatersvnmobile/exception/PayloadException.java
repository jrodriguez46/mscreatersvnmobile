package ec.com.claro.mscreatersvnmobile.exception;

import java.io.IOException;

@SuppressWarnings("serial")
public class PayloadException extends RuntimeException{
	public PayloadException(String mensaje) throws IOException{
        super(mensaje);
    }

}

